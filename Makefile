IMAGES ?= \
	  cuda \
	  torch \
	  comfyui \
	  virtio-win

REGISTRIES := \
	      registry.cn-shenzhen.aliyuncs.com/euphon-oss/euphon-images \
	      registry.gitlab.com/euphon/euphon-images
VERS = $(shell date +%Y%m%d)-$(shell git rev-parse --short HEAD)

.PHONY: FORCE

default: $(IMAGES)

push: $(addsuffix -push,$(IMAGES))

$(IMAGES): FORCE
$(IMAGES):
	for r in $(REGISTRIES); do \
		docker build -t $$r:$@-$(VERS) $@; \
	done

%-push: %
	for r in $(REGISTRIES); do \
		docker push $$r:$*-$(VERS); \
	done;

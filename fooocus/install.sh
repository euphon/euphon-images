#!/bin/bash

git clone https://github.com/lllyasviel/Fooocus.git fooocus
cd fooocus
python3 -m venv venv
source venv/bin/activate
pip install pygit2==1.12.2

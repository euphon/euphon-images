#!/bin/bash

. ./venv/bin/activate
git clone https://github.com/comfyanonymous/ComfyUI comfyui
cd comfyui
pip install -r requirements.txt
wget --quiet 'https://huggingface.co/runwayml/stable-diffusion-v1-5/resolve/main/v1-5-pruned-emaonly.ckpt?download=true' -O models/checkpoints/v1-5-pruned-emaonly.ckpt

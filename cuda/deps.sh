#!/bin/bash
set -e
# installing basicsr from requirements.txt doesn't work without torch, so
# install these first
pip3 install torch==1.13 -f https://download.pytorch.org/whl/torch_stable.html
pip3 install torchvision==0.14.0 -f https://download.pytorch.org/whl/torch_stable.html
pip3 install torchaudio==0.13.0 -f https://download.pytorch.org/whl/torch_stable.html
pip3 install -r requirements.txt
